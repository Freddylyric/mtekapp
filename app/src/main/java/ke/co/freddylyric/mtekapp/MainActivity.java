package ke.co.freddylyric.mtekapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        BottomNavigationView botNavigation = findViewById(R.id.navigation_bar);
        botNavigation.setOnNavigationItemSelectedListener(botListener);


            }
        private BottomNavigationView.OnNavigationItemSelectedListener botListener =
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        Fragment myFragment = null;

                        switch (menuItem.getItemId()) {
                            case R.id.home_nav:
                                myFragment = new HomeFragment();
                                break;

                            case R.id.notifications_nav:
                                myFragment = new NotificationsFragment();
                                break;

                            case R.id.profile_nav:
                                myFragment = new ProfileFragment();
                                break;

                            case R.id.setings_nav:
                                myFragment = new SettingsFragment();
                                break;
                        }
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                myFragment).commit();

                        return true;
                    }
                };


}
