package ke.co.freddylyric.mtekapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Button secondActivityBtn = (Button)findViewById(R.id.secondActiviyBtn);
        secondActivityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),MainActivity.class);

                startActivity(startIntent);
            }
        });

        ImageView facebookBtn = (ImageView)findViewById(R.id.facebookBtn);
        facebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),FacebookLogin.class);
                startActivity(startIntent);
            }
        });

        ImageView twitterBtn = (ImageView)findViewById(R.id.twitterBtn);
        twitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),TwitterLogin.class);
                startActivity(startIntent);
            }
        });

        ImageView gmailBtn = (ImageView)findViewById(R.id.gmailBtn);
        gmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),GmailLogin.class);
                startActivity(startIntent);
            }
        });

    }
}
